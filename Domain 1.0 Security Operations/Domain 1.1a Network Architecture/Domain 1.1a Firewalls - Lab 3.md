# Lesson 2.3 Firewall Technologies

## Lab Goals/Summary


- In this lab we will configure permit and deny statements for our Linux host using the `ufw` utility.
- In this lab we will also use command to view our host systems firewall.
- For this lab I am using Debian-based Linux OS (kali linux)

1. First we will start our linux terminal.

![](/Images/kali%20terminal.png)

2. First we will need to install the `ufw` utility. 

```
sudo apt install ufw
```

![](/Images/apt%20install%20ufw.png)

3. Next we have to actually enable the `ufw` service.

```
sudo ufw enable
```

![](/Images/ufw%20enable.png)

4. Now lets verify that the `ufw` service is running.

```
sudo systemctl enable ufw
sudo systemctl start ufw
sudo systemctl status ufw
```

![](/Images/systemctl%20ufw%20status.png)

5. We will use the `systemctl` command to verify the status of our `ufw` service. Now lets start permitting and denying services and ports. First we will allow the `openSSH` service. The `openSSH` service is a utility we can use do to remote SSH connections.

```
sudo ufw allow OpenSSH
```

![](/Images/ufw%20allow%201.png)

6. Next lets deny port 445, which is the SMB service that is a vulnerable service. We will also allow our web service/https ports - `80 | 443`

```
sudo ufw deny 445
sudo ufw allow 80
sudo ufw allow 443
```

![](/Images/ufw%20rules%201.png)

6. Now let's do some advanced configuration and only allow the `192.168.1.0/24` subnet to ssh to any destination.

```
sudo ufw allow from 192.168.1.0/24 to any port 22
```

![](/Images/ufw%20allow%202.png)

7. Before we check our firewall rules lets turn on logging so we could see all the packets our firewall denies.

```
sudo ufw logging on
```

![](/Images/ufw%20logging%20.png)

8. Now for our final step lets check our firewall rules with the `ufw status` command.f

```
sudo ufw status
```

![](/Images/ufw%20status%20command.png)

