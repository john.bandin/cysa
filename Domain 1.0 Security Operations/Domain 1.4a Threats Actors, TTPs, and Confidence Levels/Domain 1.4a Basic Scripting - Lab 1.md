# Domain 1.4a Basic Scripting Lab


## Lab Goals/Summary

- In this lab we are going to cover basic scripting in Linux. This will include special characters in scripts, how to edit script file permissions, how to create scripts using nano/vi, how to call a script from the local directory, what a string is, what an array is.
- For this lab I am using Debian-based Linux OS (kali linux)

1. First we will open our Linux terminal. 

![](/Images/kali%20terminal.png)

2. First Let's go over the basics of creating scripts on the terminal. The script we will make will create users for us. Using nano or vi create the file `create_users.sh`. Note that we need to add the `.sh` file extensions. Below is the code we will paste into our script. 

```
#!/bin/bash

# Check if script is run with root privileges
if [ "$EUID" -ne 0 ]; then
  echo "This script must be run with root privileges."
  exit 1
fi

# Loop through each argument (username) and create a user
for username in "$@"; do
  # Check if user already exists
  if id "$username" &>/dev/null; then
    echo "User $username already exists. Skipping."
  else
    # Create the user with home directory and group
    useradd -m "$username"
    echo "User $username created successfully."
  fi
done

echo "User creation process complete."

```

3. A couple things to note before we create our script. The "**#!/bin/bash**" line is called a shebang or hashbang, and it's used to specify the interpreter that should be used to execute the script. When you run a script in a Unix-like operating system (including Linux), the system uses the shebang line to determine which interpreter should be used to interpret and execute the script. 

4. The "**#**" character denotes a "**comment**". Comments are lines in your script that are not functional, they are just there to give information to the reader about the script.

5. The "**$**" characters denotes a variable. For this script we will have to pass a value to be inputted into the `$username` variable. Variables can and are sometimes supposed to change in our scripts.

6. Now Let's create our script. First copy the script from GitLab.

![](/Images/copy%20to%20clipboard.png)

```
sudo nano create_users.sh
```

![](/Images/nano%20bash%20script.png)

`Ctrl + o`

`Ctrl + x`

7. Now that we have created this script, lets make sure we give it the correct "**executable**" permissions.

```
sudo chmod +x create_users.sh
```

![](/Images/script%20executable%20right.png)

8. Now lets run our script, and give it some input so it can create users for us, and then verify that the users where created in the `/etc/passwd` file. 

```
sudo ./create_users.sh user1 user2 user3 user4 user5

cat /etc/passwd | grep user*
```

![](/Images/create%20users%20in%20bash.png)

9. Now that we have created a script and executed it, lets go over some scripting basics; let's go over what a string is using python as an example. Python is always installed with linux. To enter a python session follow the commands as we demonstrate what a string is.

```
python3
```

```
# The characters and words in between the quotes is a "string" or a "string literal"

example_string = "hello world"
print(example_string)
```
```
quit()
```

![](/Images/python%20string%20example.png)

10. As we can see from the image above when we print the variable "**example_string**" we are printing the string "**hello world**". We can also do this from the terminal itself using the `echo` command.

```
echo "hello world"
```

![](/Images/echo%20hello%20world.png)

11. Now let's cover what an "**array**" is and what it looks like. An array in programming is a data structure that holds a collection of values, each identified by an index or a key. We will create another script labeled `array_example.sh`. Copy and paste this script below into a new file we will create using `nano`.

```
#!/bin/bash

# Declare an array
fruits=("Apple" "Banana" "Orange" "Grapes" "Mango")

# Print the whole array
echo "All fruits: ${fruits[@]}"

# Print specific elements
echo "First fruit: ${fruits[0]}"
echo "Third fruit: ${fruits[2]}"

# Length of the array
echo "Number of fruits: ${#fruits[@]}"

# Loop through the array
echo "Loop through the array:"
for fruit in "${fruits[@]}"; do
  echo " - $fruit"
done
```

12. This script demonstrates the following array-related concepts in Bash:

13. Declaring an array:

```
# Declare an array
fruits=("Apple" "Banana" "Orange" "Grapes" "Mango")
```

14. Accessing array elements: ${fruits[index]} where index is the index of the element.

15. Printing the whole array: 

```
# Print the whole array
echo "All fruits: ${fruits[@]}"
```

16. Getting the length of the array:

```
# Length of the array
echo "Number of fruits: ${#fruits[@]}"
```

17. Now let's create the script, make it executable, and run the script.

![](/Images/bash%20array%20snip.png)

```
sudo nano array_example.sh
```

![](/Images/bash%20array%20nano%20script.png)

```
Ctrl + o

Ctrl + x
```

```
sudo chmod +x array_example.sh
ls -la array_example.sh
```

![](/Images/chmod%20array%20example.png)

18. Now let's run our script. 

```
sudo ./array_example.sh
```

![](/Images/array%20script%20ran.png)








